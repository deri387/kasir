<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v1/auth'], function () {
    Route::post('login', 'API\AuthController@login');
    Route::post('reset', 'API\AuthController@resetPassword');
    Route::post('reset/check', 'API\AuthController@checkTokenPasswordReset');
    Route::post('reset/save', 'API\AuthController@resetPasswordSave');
    Route::post('register', 'API\AuthController@registration');
    Route::get('logout', 'API\AuthController@logout');
    Route::get('version', 'API\AuthController@getVersion');
});

Route::group(['middleware' => ['auth:api', 'verified'], 'prefix' => 'v1'], function () {    
    Route::post('email/verify', 'API\PAGES\UsersController@updateVerify');
    Route::get('password/change','API\PAGES\UsersController@updatePassword');
});

Route::group(['middleware' => ['auth:api'], 'prefix' => 'v1'], function () {
    Route::get('user/credential', 'API\AuthController@retrieveCredential');
    Route::get('user/email/resend', 'API\AuthController@resendEmail');
});