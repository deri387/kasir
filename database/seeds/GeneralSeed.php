<?php

use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;
class GeneralSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * 
         * Base user
         */
        $user_id_1 = Uuid::uuid1()->getHex();

        DB::table('users')->insert([
            'user_id' => $user_id_1,
            'email' => 'komaraderii@gmail.com',
            'password' => bcrypt('doeraso387'),
            'email_verified_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
