<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;
use App\Notifications\VerifyEmail;
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable,HasApiTokens;
    /**
     * Assign new primary key
     */
    protected $primaryKey = 'user_id';

    /**
     * Set incrementing primary key to false
     */
    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','email_token','email_verified_at','user_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Appends attributes to object results.
     * 
     * @var array
     */
    protected $appends = [
        'is_verified'
    ];


    /**
     * Set generate uuid value user_id.
     *
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->user_id = Uuid::uuid1()->getHex();
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
    
    /**
     * Append value to 'is_verified'. Check value is not empty
     * 
     * @return Collection
     */
    protected function getIsVerifiedAttribute()
    {
        return !empty($this->attributes['email_verified_at']);
    }
    
    /**
     * Override send email verification
     * 
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }
}
