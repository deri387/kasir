<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
class ResetPassword extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($post)
    {
        $this->post = $post;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $data = $this->post;
        return (new MailMessage)
            ->subject(Lang::getFromJson('Reset Password'))
            ->greeting('Hello,')
            ->line(Lang::getFromJson('Kami mendengar bahwa anda ingin melakukan reset password. Harap klik button dibawah ini untuk melakukan reset password. Link ini hanya berlaku 1 kali.'))
            ->action(
                Lang::getFromJson('Reset Password'), url('/').'reset/'.$data->token
            )
            ->line(Lang::getFromJson('Jika anda merasa tidak melakukan reset password, abaikan pesan ini.'));
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
