<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Supplier;
use App\Reseller;
use App\PasswordReset;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;
use App\Notifications\ResetPassword;
class AuthController extends Controller
{
    protected $GTOKEN = ''; //recapthca api key server

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('guest')->except(['retrieveCredential', 'retrieveSubscribed', 'resendEmail']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'token' => 'required|string',
            'password' => 'required|string|min:8|confirmed',
        ]);
    }

     /**
     * Registration callback.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function registration(Request $request) 
    {
        $validate = $this->validator($request->all());
        if ($validate->fails()) {
            return response()->json([
                'message' => 'Gagal mendaftar.',
                'serve' => [
                    'error' => $validate->errors()
                ]
            ], 400);
        }

        $clientIps = request()->getClientIps();
        $visitorIp = end($clientIps);

        $client = new Client();
        $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
            'form_params' => [
                'secret' => $this->GTOKEN,
                'response' => $request->token,
                'remoteip' => $visitorIp
            ]
        ]);
        $dirtyResult = $response->getBody()->getContents();
        $result = json_decode($dirtyResult, true);
        
        if (!$result['success']) {
            return response()->json([
                'message' => 'Deleteing your data at ' . $visitorIp . '. Good luck.'
            ], 400);
        }
        
        $dataUser = new User();
        $dataUser->email = $request->email;
        $dataUser->password = bcrypt($request->password);
        $dataUser->email_token = base64_encode($request->email);
        $dataUser->save();
        $dataUser->sendEmailVerificationNotification();
        $success = [
            'message' => 'Anda berhasil mendaftar, silahkan lakukan konfirmasi email untuk memastikan profile anda benar.',
            'serve' => []
        ];
        return response()->json($success, 200);
    }
    /**
     * Login callback.
     * 
     * @param \Illuminate\Http|Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Username atau password anda tidak tepat.',
                'serve' => []
            ], 400);

        $user = Auth::user();
        if($user->status == 0){
            return response()->json([
                'message' => 'Kami mendeteksi bahwa akun anda telah melakukan pelanggaran yang merugikan pihak nonce maka dari itu kami memutuskan untuk menonaktifkan akun anda.',
                'serve' => []
            ], 400);
        }
        $now = \Carbon\Carbon::now();
        
        $tokenResult = $user->createToken('nonce-'.$now.'-'.$user->email);
        $token = $tokenResult->token;        
        $token->save();

        return response()->json([
            'message' => 'Login sukses.',
            'serve' => [
                'access_token' => $tokenResult->accessToken,
                'user' => $user
            ]
        ], 200);
    }

    /**
     * Logout user and revoke token
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::logout();
        return response()->json([
            'message' => 'Logout sukses.'
        ], 200);
    }

    /**
     * Resend verification email for authenticated user
     * 
     * @return void
     */
    public function resendEmail()
    {
        $user = Auth::user();
        $user->sendEmailVerificationNotification();
        return response()->json([
            'message' => 'Sukses.'
        ], 200);
    }

    /**
     * get version
     * 
     * @return void
     */
    public function getVersion()
    {
        $version = '1.0.0';
        return response()->json([
            'message' => 'Sukses.',
            'serve' => $version
        ], 200);
    }

     /**
     * Reset password
     * 
     * @return void
     */
    public function resetPassword(Request $request)
    {
        $user = User::where('email',$request->email)->first();
        if(!$user){
            return response()->json([
                'message' => 'Unauthenticated user.',
                'serve' => []
            ], 401);
        }
        $passwordreset = PasswordReset::where('email',$request->email)->first();
        if($passwordreset){
            return response()->json([
                'message' => "Kami sudah mengirimkan link pembaharuan ke email tersebut.",
                'serve' => []
            ], 400);
        }
        $passwordreset = new PasswordReset();
        $passwordreset->email = $request->email;
        $passwordreset->token = Uuid::uuid1()->getHex();
        $passwordreset->save();
        $passwordreset->notify(new ResetPassword($passwordreset));
        return response()->json([
            'message' => 'Berhasil.',
            'serve' => []
        ], 200);
    }

    /**
     * Check token password reset and remove data by token
     * 
     * @return void
     */
    public function checkTokenPasswordReset(Request $request)
    {
        $passwordreset = PasswordReset::where('token',$request->token)->first();
        if($passwordreset){
            $delete = PasswordReset::where('token',$request->token)->delete();
            if($delete){
                return response()->json([
                    'message' => 'Sukses.',
                    'serve' => $passwordreset->email
                ], 200);
            }
        }
        return response()->json([
            'message' => 'Sukses.',
            'serve' => false
        ], 200);
    }
    /**
     * Check token password reset and remove data by token
     * 
     * @return void
     */
    public function resetPasswordSave(Request $request)
    {
        $passwordreset = User::where('email',$request->email)->first();
        if($passwordreset){
            $passwordreset->password = bcrypt($request->password);
            $passwordreset->save();
            if($passwordreset){
                return response()->json([
                    'message' => 'Sukses.',
                    'serve' => true
                ], 200);
            }
        }
        return response()->json([
            'message' => 'Sukses.',
            'serve' => false
        ], 200);
    }

    /**
     * Get authenticated user.
     * 
     * @return \Illuminate\Http\Response
     */
    public function retrieveCredential()
    {
        return response()->json([
            'message' => 'Sukses.',
            'serve' => Auth::user()
        ], 200);
    }
}
