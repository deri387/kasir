<?php

namespace App\Http\Controllers\API\PAGES;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UsersController extends Controller
{
    /**
     * return update password
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request)
    {
        try {
            $id = \Auth::user()->id;
            $oldPass = $request->old_pass;
            $newPass = $request->new_pass;
            $updateUser = User::find($id);
    
            if (!$updateUser){
                return response()->json([
                    'message' => 'Unauthenticated user.', 
                    'serve' => []
                ], 500);
            } else {
                if (password_verify($oldPass, $updateUser->password)){
                    if ($oldPass == $newPass) {
                        return response()->json([
                            'message' => 'Password baru dan konfirmasi password tidak sama.',
                            'serve' => []
                        ], 400);
                    }
                    $updateUser->password = bcrypt($newPass);
                    $updateUser->save();

                    return response()->json([
                        'message' => 'Password berhasil diubah.',
                        'serve' => $updateUser
                    ], 200);             
                }   
                else{
                    return response()->json([
                        'message' => 'Password lama anda salah atau tidak valid',
                        'serve' => []
                    ], 400);
                }
            }
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'Terjadi kesalahan dengan server, silahkan coba lagi.',
                'serve' => []
            ], 500);
        }
    }

    /**
     * Return update verify
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateVerify(Request $request)
    {   
        try {
            $id = $request->user_id;
            $updateUser = User::find($id);

            if (!$updateUser){
                return response()->json([
                    'message' => 'Unauthenticated user.',
                    'serve'   => []
                ], 400);
            } else {
                $updateUser->email_verified_at = !$updateUser->email_verified_at ? \Carbon\Carbon::now() : null;
                $updateUser->status = 1;
                $updateUser->save();

                return response()->json([
                    "message" => 'Berhasil terverifikasi.',
                    "serve" => $updateUser
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'Terjadi kesalahan dengan server, silahkan coba lagi.',
                'serve' => []
            ], 500);
        }
    }
}
